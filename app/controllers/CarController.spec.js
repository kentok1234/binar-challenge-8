const CarController = require("./CarController")
const {Car, UserCar} = require("../models")
const dayjs = require("dayjs");
const { CarAlreadyRentedError } = require("../errors");

describe("CarController", () => {
    describe("#handleListCars", () => {
        it("Should return status 200 and json cars and meta object.", async () => {
            const mockRequest = {
                query: {
                    page: 1,
                    pageSize: 10,
                    size: "",
                    availableAt: true
                }
            }

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis()
            }

            const name = "Honda Jazz"
            const price = 15000
            const size = "SMALL"
            const image = "https://source.unsplash.com/517x517"
            const isCurrentlyRented = false

            const cars = []

            for (let i = 0; i < 10; i++) {
                const car = new Car({
                    name,
                    price,
                    size,
                    image,
                    isCurrentlyRented
                })
                cars.push(car)
            }

            const mockCarModel = {
                findAll: jest.fn().mockReturnValue(cars),
                count: jest.fn().mockReturnValue(10)
            }



            const carController = new CarController({carModel: mockCarModel, userCarModel:UserCar, dayjs})
            await carController.handleListCars(mockRequest, mockResponse)

            expect(mockCarModel.findAll).toHaveBeenCalled()
            expect(mockCarModel.count).toHaveBeenCalled()
            expect(mockResponse.status).toHaveBeenCalledWith(200)
            expect(mockResponse.json).toHaveBeenCalledWith({
                cars,
                meta: {
                    pagination: {
                        page: 1,
                        pageCount: 1,
                        pageSize: 10,
                        count: 10,
                    }
                }
            })
        })
    })

    describe("#handleGetCar", () => {
        it("should return respon status 200 and json car", async () => {
            const name = "Honda Jazz"
            const price = 15000
            const size = "SMALL"
            const image = "https://source.unsplash.com/517x517"
            const isCurrentlyRented = false
            
            const mockRequest = {
                params: {
                    id: 1
                }
            }

            const mockCar = new Car({name, price, size, image, isCurrentlyRented})
            const mockCarModel = {
                findByPk: jest.fn().mockReturnValue(mockCar)
            }

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis()
            }

            const carController = new CarController({carModel:mockCarModel, userCarModel:UserCar, dayjs})
            await carController.handleGetCar(mockRequest, mockResponse)

            expect(mockCarModel.findByPk).toHaveBeenCalledWith(1)
            expect(mockResponse.status).toHaveBeenCalledWith(200)
            expect(mockResponse.json).toHaveBeenCalledWith(mockCar)
        })
    })

    describe("#handleCreateCar", () => {
        it("should return status 201 and json car", async () => {
            const name = "Honda Jazz"
            const price = 15000
            const size = "SMALL"
            const image = "https://source.unsplash.com/517x517"

            const mockRequest = {
                body: {
                    name,
                    price,
                    size,
                    image
                }
            }

            const car = new Car({name, price, size, image, isCurrentlyRented: false})
            const mockCarModel = {
                create: jest.fn().mockReturnValue(car)
            }

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis()
            }

            const carController = new CarController({carModel: mockCarModel, userCarModel: UserCar, dayjs})
            await carController.handleCreateCar(mockRequest, mockResponse)
            
            expect(mockCarModel.create).toHaveBeenCalled()
            expect(mockResponse.status).toHaveBeenCalledWith(201)
            expect(mockResponse.json).toHaveBeenCalledWith(car)
        })

        it("should return status 422 and json error object", async () => {
            const err = new Error("Something Error")

            const name = "Honda Jazz"
            const price = 15000
            const size = "SMALL"
            const image = "https://source.unsplash.com/517x517"

            const mockRequest = {
                body: {
                    name,
                    price,
                    size,
                    image
                }
            }

            const mockCarModel = {
                create: jest.fn().mockReturnValue(Promise.reject(err))
            }

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis()
            }

            const carController = new CarController({carModel: mockCarModel, userCarModel: UserCar, dayjs})
            await carController.handleCreateCar(mockRequest, mockResponse)

            expect(mockResponse.status).toHaveBeenCalledWith(422)
            expect(mockResponse.json).toHaveBeenCalledWith({
                error: {
                    name: err.name,
                    message: err.message,
                }
            })
        })
    })

    describe("#handleRentCar", () => {
        it("should return status 201 and json user car rental.", async () => {
            const name = "Honda Jazz"
            const price = 15000
            const size = "SMALL"
            const image = "https://source.unsplash.com/517x517"
            const isCurrentlyRented = false

            const mockRequest = {
                body: {
                    rentStartedAt: dayjs().toISOString()
                },
                params: {
                    id: 1
                },
                user: {
                    id: 1,
                }
            }

            const mockCar = new Car({id: 1, name, price, size, image, isCurrentlyRented})
            const mockCarModel = {
                findByPk: jest.fn().mockReturnValue(mockCar)
            }

            const userId = mockRequest.user.id
            const carId = mockCar.id
            const rentStartedAt = mockRequest.body.rentStartedAt
            const rentEndedAt = dayjs(rentStartedAt).add(1, "day")
            
            const mockUserCar = new UserCar({userId, carId, rentStartedAt, rentEndedAt})
            const mockUserCarModel = {
                findOne: jest.fn().mockReturnValue(false),
                create: jest.fn().mockReturnValue(mockUserCar)
            }

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis()
            }

            const carController = new CarController({carModel:mockCarModel, userCarModel:mockUserCarModel, dayjs})
            await carController.handleRentCar(mockRequest, mockResponse)

            expect(mockCarModel.findByPk).toHaveBeenCalled()
            expect(mockUserCarModel.findOne).toHaveBeenCalled()
            expect(mockUserCarModel.create).toHaveBeenCalled()
            expect(mockResponse.status).toHaveBeenCalledWith(201)
            expect(mockResponse.json).toHaveBeenCalledWith(mockUserCar)

        })

        it("should return status 422 and json error", async () => {
            const name = "Honda Jazz"
            const price = 15000
            const size = "SMALL"
            const image = "https://source.unsplash.com/517x517"
            const isCurrentlyRented = false

            const mockRequest = {
                body: {
                    rentStartedAt: dayjs().toISOString()
                },
                params: {
                    id: 1
                },
                user: {
                    id: 1,
                }
            }

            const mockCar = new Car({id: 1, name, price, size, image, isCurrentlyRented})
            const mockCarModel = {
                findByPk: jest.fn().mockReturnValue(mockCar)
            }

            const userId = mockRequest.user.id
            const carId = mockCar.id
            const rentStartedAt = mockRequest.body.rentStartedAt
            const rentEndedAt = dayjs(rentStartedAt).add(1, "day")
            
            const mockUserCar = new UserCar({userId, carId, rentStartedAt, rentEndedAt})
            const mockUserCarModel = {
                findOne: jest.fn().mockReturnValue(true),
                create: jest.fn().mockReturnValue(mockUserCar)
            }

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis()
            }

            const err = new CarAlreadyRentedError(mockCar)

            const carController = new CarController({carModel:mockCarModel, userCarModel:mockUserCarModel, dayjs})
            await carController.handleRentCar(mockRequest, mockResponse)

            expect(mockCarModel.findByPk).toHaveBeenCalled()
            expect(mockUserCarModel.findOne).toHaveBeenCalled()
            expect(mockUserCarModel.create).toHaveBeenCalled()
            expect(mockResponse.status).toHaveBeenCalledWith(422)
            expect(mockResponse.json).toHaveBeenCalledWith(err)
        })
    })

    describe("#handleUpdateCar", () => {
        it("should return status 200 and json car", async () => {
            const name = "Honda Jazz"
            const price = 15000
            const size = "SMALL"
            const image = "https://source.unsplash.com/517x517"
            const isCurrentlyRented = false

            const mockRequest = {
                body: {
                    name: "Tesla 3000",
                    price,
                    size,
                    image,
                    isCurrentlyRented,
                },
                params: {
                    id: 1
                }
            }

            const mockCar = new Car({id: 1, name, price, size, image, isCurrentlyRented})
            const mockCarModel = {
                findByPk: jest.fn().mockReturnValue(mockCar),
                update: jest.fn().mockReturnValue(mockRequest.body)
            }

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis()
            }

            const carController = new CarController({carModel: mockCarModel, userCarModel: UserCar, dayjs})
            await carController.handleUpdateCar(mockRequest, mockResponse)

            expect(mockCarModel.findByPk).toHaveBeenCalled()
            expect(mockCarModel.update).toHaveBeenCalled()
            expect(mockResponse.status).toHaveBeenCalledWith(200)
            expect(mockResponse.json).toHaveBeenCalledWith(mockRequest.body)
        })

        it("should return status 422 and json error object.", async () => {
            const err = new Error("Something Error")

            const name = "Honda Jazz"
            const price = 15000
            const size = "SMALL"
            const image = "https://source.unsplash.com/517x517"

            const mockRequest = {
                body: {
                    name,
                    price,
                    size,
                    image
                },
                params: {
                    id: 1
                }
            }

            const mockCarModel = {
                findByPk: jest.fn().mockReturnValue(Promise.reject(err)),
                update: jest.fn().mockReturnValue(mockRequest.body)
            }

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis()
            }

            const carController = new CarController({carModel: mockCarModel, userCarModel: UserCar, dayjs})
            await carController.handleUpdateCar(mockRequest, mockResponse)

            expect(mockCarModel.findByPk).toHaveBeenCalled()
            expect(mockCarModel.update).toHaveBeenCalled()
            expect(mockResponse.status).toHaveBeenCalledWith(422)
            expect(mockResponse.json).toHaveBeenCalledWith({
                error: {
                    name: err.name,
                    message: err.message,
                }
            })
        })
    })

    describe("#handleDeleteCar", () => {
        it("should return status 204 and end", async () => {
            const mockRequest = {
                params: {
                    id: 1
                }
            }

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                end: jest.fn().mockReturnThis()
            }

            const name = "Honda Jazz"
            const price = 15000
            const size = "SMALL"
            const image = "https://source.unsplash.com/517x517"
            const isCurrentlyRented = false

            const mockCar = new Car({id: 1, name, price, size, image, isCurrentlyRented})
            const mockCarModel = {
                destroy: jest.fn().mockReturnValue(mockCar),
            }

            const carController = new CarController({carModel: mockCarModel, userCarModel: UserCar, dayjs})
            await carController.handleDeleteCar(mockRequest, mockResponse)

            expect(mockCarModel.destroy).toHaveBeenCalled()
            expect(mockResponse.status).toHaveBeenCalledWith(204)
            expect(mockResponse.end).toHaveBeenCalled()
        })
    })

    describe("#getCarFromRequest", () => {
        it("should return car by id", async () => {
            const name = "Honda Jazz"
            const price = 15000
            const size = "SMALL"
            const image = "https://source.unsplash.com/517x517"
            const isCurrentlyRented = false

            const mockCar = new Car({id: 1, name, price, size, image, isCurrentlyRented})
            const mockCarModel = {
                findByPk: jest.fn().mockReturnValue(mockCar),
            }

            const mockRequest = {
                params: {
                    id: 1
                }
            }

            const mockResponse = {}

            const carController = new CarController({carModel: mockCarModel, userCarModel: UserCar, dayjs})
            const result = await carController.getCarFromRequest(mockRequest)

            expect(mockCarModel.findByPk).toHaveBeenCalled()
            expect(result).toEqual(mockCar)
        })
    })
})