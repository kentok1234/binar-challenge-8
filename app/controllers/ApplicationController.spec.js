const ApplicationController = require('./ApplicationController')
const { NotFoundError } = require("../errors");

describe("ApplicationController", () => {
    describe("#handleGetRoot", () => {
        it("should res status 200 with json status OK and message BCR API is up and running!", async () => {
            const status = "OK"
            const message = "BCR API is up and running!"
            const mockRequest = {}
            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis()
            }
            
            const application = new ApplicationController()
            await application.handleGetRoot(mockRequest, mockResponse)

            expect(mockResponse.status).toHaveBeenCalledWith(200)
            expect(mockResponse.json).toHaveBeenCalledWith({
                status,
                message
            })
        })
    })

    describe("#handleNotFound", () => {
        it("should status 404 and return json error name, message, and detail", async () => {
            const mockRequest = {
                method: "GET",
                url: "http://localhost:8000/somethingerrorurl"
            }
            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis()
            }
            
            const application = new ApplicationController()
            await application.handleNotFound(mockRequest, mockResponse)
            const err = new NotFoundError(mockRequest.method, mockRequest.url)

            expect(mockResponse.status).toHaveBeenCalledWith(404)
            expect(mockResponse.json).toHaveBeenCalledWith({
                error: {
                    name: err.name,
                    message: err.message,
                    details: err.details,
                }
            })
        })
    })

    describe("#handleError", () => {
        it("should response status 500 with json error name, message and details", async () => {
            const err = new Error("Something Error")

            const mockRequest = {}
            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis()
            }

            const application = new ApplicationController()
            await application.handleError(err, mockRequest, mockResponse)

            expect(mockResponse.status).toHaveBeenCalledWith(500)
            expect(mockResponse.json).toHaveBeenCalledWith({
                error: {
                    name: err.name,
                    message: err.message,
                    details: err.details || null,
                }
            })
        })
    })

    describe("#getOffsetFromRequest", () => {
        it("should return offset", async () => {
            const mockRequest = {
                query: {
                    page: Math.floor(Math.random() * (10 - 1) + 1),
                    pageSize: Math.floor(Math.random() * (100 - 10) + 10)
                }
            }

            const application = new ApplicationController()
            const result = await application.getOffsetFromRequest(mockRequest)
            expect(result).toBe(
                (mockRequest.query.page - 1) * mockRequest.query.pageSize
            )

        })
    })

    describe("#buildPaginationObject", () => {
        it("should return page, pageCount, pageSize, and count", async () => {
            const mockRequest = {
                query: {
                    page: Math.floor(Math.random() * (10 - 1) + 1),
                    pageSize: Math.floor(Math.random() * (100 - 10) + 10)
                }
            }

            const mockCount = Math.floor(Math.random() * (100 - 1) + 1)

            const application = new ApplicationController()
            const result = await application.buildPaginationObject(mockRequest, mockCount)

            expect(result.page).toBe(mockRequest.query.page)
            expect(result.pageCount).toBe(Math.ceil(mockCount / mockRequest.query.pageSize))
            expect(result.pageSize).toBe(mockRequest.query.pageSize)
            expect(result.count).toBe(mockCount)
        })
    })
})