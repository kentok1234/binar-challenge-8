const AuthenticationController = require("./AuthenticationController")
const ApplicationController = require("./ApplicationController");
const { EmailNotRegisteredError, InsufficientAccessError, RecordNotFoundError, WrongPasswordError } = require("../errors");
const { JWT_SIGNATURE_KEY } = require("../../config/application");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const {
    User,
    Role,
  } = require("../models");

describe("AuthenticationController", () => {
    describe("#handleRegister", () => {
        it("should return status 201 and json access token.", async () => {
            const name = "Something name"
            const email = "something@email.com"
            const password = "password"
            const encryptedPassword = bcrypt.hashSync(password, 10)
            const roleId = 3
            
            const role = new Role({id: roleId, name: "CUSTOMER"})
            const user = new User({name, email, encryptedPassword, roleId})

            const mockRoleModel = {
                findOne: jest.fn().mockReturnValue(role)
            }

            const mockUserModel = {
                create: jest.fn().mockReturnValue(user),
                findOne: jest.fn().mockReturnValue(false)
            }

            const token = jwt.sign({
                id: user.id,
                name: user.name,
                email: user.email,
                image: user.image,
                role: {
                    id: role.id,
                    name: role.name,
                }
            }, JWT_SIGNATURE_KEY)

            const mockRequest = {
                body: {
                    name,
                    email,
                    password
                }
            }

            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis()
            }

            const authentication = new AuthenticationController({userModel: mockUserModel, roleModel: mockRoleModel, bcrypt, jwt})
            await authentication.handleRegister(mockRequest, mockResponse)

            expect(mockUserModel.findOne).toHaveBeenCalled()
            expect(mockRoleModel.findOne).toHaveBeenCalled()
            expect(mockUserModel.create).toHaveBeenCalled()
            expect(mockResponse.status).toHaveBeenCalledWith(201)
            expect(mockResponse.json).toHaveBeenCalledWith({
                accessToken: token,
            })
        })
    })
})