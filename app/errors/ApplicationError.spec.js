const ApplicationError = require('./ApplicationError')

describe("ApplicationError", () => {
    describe("#getdetails", () => {
        it("Should return empty object", () => {
            const err = new ApplicationError("Something Error")

            expect(err).toBeInstanceOf(Error)
            expect(Object.getPrototypeOf(err.details)).toEqual(Object.prototype)
        })
    })
    describe("#toJSON", () => {
        it("Should return error object", () => {
            const err = new ApplicationError("Something Error")
            const mockReturnValue = jest.fn(() => err.toJSON())
            
            mockReturnValue()

            expect(mockReturnValue).toHaveReturned()
            expect(mockReturnValue).toHaveReturnedWith({
                error: {
                    name: "Error",
                    message: "Something Error",
                    details: {}
                }
            })
        })
    })
})